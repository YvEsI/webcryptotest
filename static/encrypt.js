function startsubmit() {
	var file = document.getElementById('preupload').files[0]; //Files[0] = 1st file
	var reader = new FileReader();
	reader.readAsBinaryString(file);
	reader.onload = shipOff;
	//TODO: implement the following
	//reader.onloadstart = ...
	//reader.onprogress = ... <-- Allows you to update a progress bar.
	//reader.onabort = ...
	//reader.onerror = ...
	//reader.onloadend = ...
}

function shipOff(event) {
    var result = event.target.result;
	var myform = document.forms.namedItem("upload");
	var oOutput = document.getElementById("status")
	var oData = new FormData(myform);
	//TODO: Generate filename
    var fileName = 'test1'
	//TODO: Encrypt the data
	console.log(document.getElementById('preupload').files[0]);
	var encryptedData = result
	console.log(encryptedData);
	var oReq = new XMLHttpRequest();
	oReq.open("POST", "/upload", true);
	oReq.onload = function(oEvent) {
		if (oReq.status == 200) {
			oOutput.innerHTML = "Uploaded!";
			console.log(oReq.response);
		} else {
			oOutput.innerHTML = "Error " + oReq.status + " occurred when trying to upload your file.<br \/>";
		}
	};
	oData.append("upload", encryptedData)
	oData.append("uploadName", fileName)
	oReq.send(oData);
}