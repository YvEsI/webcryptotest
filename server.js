var formidable = require('formidable'),
    http = require('http'),
    util = require('util'),
	contentDisposition = require('content-disposition'),
	finalhandler = require('finalhandler'),
	serveStatic = require('serve-static');

var serve = serveStatic('static', {'index': 'index.html'})

function upload(req, res) {
	// parse a file upload
    var form = new formidable.IncomingForm();
	form.parse(req, function(err, fields, files) {
		res.writeHead(200, {'content-type': 'text/plain'});
		res.write('received upload:\n\n');
		res.end(util.inspect({fields: fields}));
	});

    return;
}

function index(req, res) {
	res.writeHead(200, {'content-type': 'text/html'});
	res.end(
		'<form action="/upload" enctype="multipart/form-data" method="post">'+
		'<input type="text" name="title"><br>'+
		'<input type="file" name="upload" multiple="multiple"><br>'+
		'<input type="submit" value="Upload">'+
		'</form>'
	);
}

http.createServer(function(req, res) {
	if (req.url == '/upload' && req.method.toLowerCase() == 'post') {
		upload(req, res);
	} else {
		//index(req, res);
		serve(req, res, finalhandler(req, res));
	}
}).listen(8080);